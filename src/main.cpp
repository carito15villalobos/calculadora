#include <iostream>

using namespace std;

void calculadora() {
    int n;
    cout<<"Bienvenido a tu calculadora"<<endl;
    cout<<"1. Sumar"<<endl;
    cout<<"2. Restar"<<endl;
    cout<<"3. Multiplicar"<<endl;
    cout<<"4. Division"<<endl;
    cout<<"5. Salir"<<endl;
    cout<<"Ingrese una opcion: ";
    std::cin >> n;

    if(n==1){
       int r;
       int c;
       r=0;
       cout<<"Ingrese la cantidad de numeros que quiere sumar: ";
       std::cin >> c;
       cout<<"Ingrese uno por uno los numeros que desea sumar: "<<endl;
       for(int i = 0; i<c; i++) {
           int ns;
           cout<<"numero [";
           cout<<i+1;
           cout<<"]: ";
           std::cin>>ns;
           r = r + ns;
       }
       cout<<"El resultado es: "<<r<<endl;
       calculadora();
    }else if(n==2){
        int r;
        cout<<"Ingrese el numero para restar: ";
        int d1;
        std::cin>>d1;
        cout<<"Ingrese el numero que quiere restar: ";
        int d2;
        std::cin>>d2;
        r = d1 - d2;
        cout<<"El resultado es: "<<r<<endl;
        calculadora();
    }else if(n==3){
        int r;
        int c;
        r=1;
        cout<<"Ingrese la cantidad de numeros que quiere multiplicar: ";
        std::cin >> c;
        cout<<"Ingrese uno por uno los numeros que desea sumar: "<<endl;
        for(int i = 0; i<c; i++) {
           int ns;
           cout<<"numero [";
           cout<<i+1;
           cout<<"]: ";
           std::cin>>ns;
           r = r * ns;
        }
        cout<<"El resultado es: "<<r<<endl;
        calculadora();
    }else if(n==4){
        int r;
        cout<<"Ingrese el Dividendo: ";
        int d1;
        std::cin>>d1;
        cout<<"Ingrese el divisor: ";
        int d2;
        std::cin>>d2;
        r = d1 / d2;
        cout<<"El resultado es: "<<r<<endl;
        calculadora();
    }else if(n==5){
    }else {
        cout<<"La opcion no es valida. Intente de nuevo"<<endl;
        calculadora();
    }
}

int main()
{
    calculadora();
    return 0;
}
